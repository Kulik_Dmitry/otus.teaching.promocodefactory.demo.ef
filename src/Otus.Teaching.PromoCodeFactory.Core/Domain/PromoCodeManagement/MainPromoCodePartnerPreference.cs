﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class MainPromoCodePartnerPreference
    {
        public Guid PartnerId { get; set; }
        public virtual Partner Partner { get; set; }

        public Guid PreferenceId { get; set; }
        public virtual Preference Preference { get; set; }

    }
}